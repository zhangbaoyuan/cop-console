package com.cop.console.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by zhangbaoyuan on 2018/2/2.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
                .antMatchers("/images/**", "/css/**", "/js/**", "/fronts/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll();
//        http
//            .authorizeRequests()
//                .antMatchers( "/", "/index").permitAll()
//                .antMatchers("/admin/**").hasRole("ADMIN")
//                .antMatchers("/db/**").access("hashRole('ADMIN') and hasRole('DBA')")
//                .anyRequest().authenticated()
//                .and()
//            .formLogin()
//                .usernameParameter("username")
//                .passwordParameter("password")
//                .failureForwardUrl("/login?error")
//                .loginPage("/login")
//                .defaultSuccessUrl("/index")
//                .permitAll()
//                .and()
//            .logout()
//                .logoutUrl("/logout")
//                .logoutSuccessUrl("/index")
//                .permitAll()
//                .and()
//            .rememberMe()
//                .tokenValiditySeconds(1209600)
//                .key("USER_NAME")
//                .and()
//            .httpBasic()
//                .disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
        builder
            .inMemoryAuthentication()
                .withUser("admin")
                .password("admin")
                .roles("USER");
    }
}
