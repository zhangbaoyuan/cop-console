package com.cop.console.config;

import com.cop.console.interceptor.VisitInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by zhangbaoyuan on 2017/11/24.
 */
@Configuration
public class MyWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("/display/viewIndex");
        registry.addViewController("/index").setViewName("/display/viewIndex");
        registry.addViewController("/login").setViewName("/website/login");
        registry.addViewController("/toLeftSidebar").setViewName("/website/leftSidebar");
        registry.addViewController("/toRightSidebar").setViewName("/website/rightSidebar");
        registry.addViewController("/toNoSidebar").setViewName("/website/noSidebar");
        registry.addViewController("/viewIndex").setViewName("/display/viewIndex");
        registry.addViewController("/toViewDetail1").setViewName("/display/viewDetail1");
        registry.addViewController("/toViewDetail2").setViewName("/display/viewDetail2");
        super.addViewControllers(registry);
    }

    @Bean
    public VisitInterceptor visitInterceptor(){
        return new VisitInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(visitInterceptor());
    }
}
