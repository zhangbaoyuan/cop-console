package com.cop.console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CopConsoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CopConsoleApplication.class, args);
	}
}
